################################################################################
##########                                                            ##########
##########             Connaissance et intention de recours           ##########
##########                   au panier 100% Sant�                     ##########
##########                                                            ##########
################################################################################

################################################################################
#
# Copyright (C) 2022. Logiciel �labor� par l'�tat, via la Drees.
#
# Nom de l'auteur : Rapha�l Lardeux, Drees.
#
# Ce programme informatique a �t� d�velopp� par la Drees. Il permet de produire 
# les figures publi�es dans l'�tudes et R�sultats 1236, intitul� � 100% sant� : 
# fin 2021, un peu plus de la moiti� des Fran�ais en ont entendu parler �, 
# juillet 2022. 
#
# Ce programme a �t� ex�cut� le 07/07/2022 avec la version 4.1.2 de R et les 
# packages : xlsx_0.6.5, questionr_0.7.6, haven_2.4.3, data.table_1.14.2.
#
# Le texte et les figures de l'�tude peuvent �tre consult�s sur le site de la 
# DREES : https://drees.solidarites-sante.gouv.fr/publications-communique-de-presse/etudes-et-resultats/100-sante-fin-2021-un-peu-plus-de-la-moitie
# 
# Ce programme utilise les mill�simes 2020 et 2021 du Barom�tre d'opinion de la 
# Drees: https://drees.solidarites-sante.gouv.fr/sources-outils-et-enquetes/le-barometre-dopinion-de-la-drees
#
# Bien qu'il n'existe aucune obligation l�gale � ce sujet, les utilisateurs de 
# ce programme sont invit�s � signaler � la DREES leurs travaux issus de la 
# r�utilisation de ce code, ainsi que les �ventuels probl�mes ou anomalies 
# qu'ils y rencontreraient, en �crivant � DREES-CODE@sante.gouv.fr
# 
# Ce logiciel est r�gi par la licence "GNU General Public License" GPL-3.0. 
# https://spdx.org/licenses/GPL-3.0.html#licenseText
# 
# � cet �gard l'attention de l'utilisateur est attir�e sur les risques associ�s
# au chargement, � l'utilisation, � la modification et/ou au d�veloppement et �
# la reproduction du logiciel par l'utilisateur �tant donn� sa sp�cificit� de 
# logiciel libre, qui peut le rendre complexe � manipuler et qui le r�serve donc 
# � des d�veloppeurs et des professionnels avertis poss�dant des connaissances 
# informatiques approfondies. Les utilisateurs sont donc invit�s � charger et 
# tester l'ad�quation du logiciel � leurs besoins dans des conditions permettant 
# d'assurer la s�curit� de leurs syst�mes et ou de leurs donn�es et, plus 
# g�n�ralement, � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�.
# 
# Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez pris 
# connaissance de la licence GPL-3.0, et que vous en avez accept� les termes.
#
# This program is free software: you can redistribute it and/or modify it under 
# the terms of the GNU General Public License as published by the Free Software 
# Foundation, either version 3 of the License, or (at your option) any later 
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT 
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with 
# this program. If not, see <https://www.gnu.org/licenses/>.
#
################################################################################


rm(list=ls())

library(data.table)
library(haven)
library(questionr)
library(xlsx)

path <- "C://Users//raphael.lardeux//Documents//Barometre//Data//bases unifiees//"


### Bases historiques
baro        <- as.data.table(readRDS(paste0(path,"baro2000_2021.rds")))
names(baro) <- tolower(names(baro))

# Base 2020 pour la connaissance des aides et prestations sociales:
baro_2020         <- as.data.table(readRDS(paste0(path,"baro2000_2020.rds")))
names(baro_2020)  <- tolower(names(baro_2020))


### Fonction:
stars <- function(x){ifelse(x<=0.01,"***",ifelse(x>0.01 & x<=0.05,"**",ifelse(x>0.05 & x<=0.1,"*","")))}

### Dossiers de sortie:
dir.create("sorties", showWarnings = F)       # dossier de sorties





################################################################################
##########                    Cr�ation de variables                   ##########
################################################################################


### Variables g�n�riques:

# Dipl�me:
baro[, dipl := ifelse(sddipl==1,1,
                      ifelse(sddipl%in%c(2,3,4),2,
                             ifelse(sddipl%in%c(5,6,7,12),3,
                                    ifelse(sddipl%in%c(8,9),4,NA))))]
# Groupe d'age :
baro[, age := ifelse(sdage < 30,1,ifelse(sdage < 45,2, ifelse(sdage < 65,3,4)))]

# Nbr d'enfants
baro[, sdnbenf := sdnbenf - 1]
baro[, enf := ifelse(sdnbenf == 0,0,ifelse(sdnbenf==1,1,2))]

# Famille
baro[, famille := ifelse(sdsitfam==1 & sdnbenf==0,1,
                         ifelse(sdsitfam==2 & sdnbenf==0,2,
                                ifelse(sdsitfam==2 & sdnbenf!=0,3,
                                       ifelse(sdsitfam==3 & sdnbpers!=0,4,5))))]

# Statut d'occupation du logement
baro[, log:= ifelse(lo1==5,NA,lo1)]

# 1 = propri 
# 2 = locataire social
# 3 = locataire priv�
# 4 = gratuit


### Variables de sant�:

# Etat de sant�:
baro[, etat_sante := ifelse(sa1 %in% c(1,2),1,ifelse(sa1==6,NA,0))]

# Renoncement aux soins:
baro[, besoin_exam := ifelse(sa27%in%c(2,3),1,ifelse(sa27==4,NA,0))]
baro[, renonc_exam := ifelse(sa27==2,1,ifelse(sa27==3,0,NA))]
baro[, besoin_dent := ifelse(me3_1%in%c(2,3),1,ifelse(me3_1==4,NA,0))]
baro[, renonc_dent := ifelse(me3_1==2,       1,ifelse(me3_1==3,0,NA))]
baro[, besoin_optic := ifelse(me3_2%in%c(2,3),1,ifelse(me3_2==4,NA,0))]
baro[, renonc_optic := ifelse(me3_2==2,       1,ifelse(me3_2==3,0,NA))]
baro[, besoin_audio := ifelse(me3_3%in%c(2,3),1,ifelse(me3_3==4,NA,0))]
baro[, renonc_audio := ifelse(me3_3==2,       1,ifelse(me3_3==3,0,NA))]
baro[, besoin_100p := ifelse(besoin_optic == 1 | besoin_dent == 1 | besoin_audio == 1,1,0)]
baro[, renonc_100p := ifelse(renonc_optic == 1 | renonc_dent == 1 | renonc_audio == 1,1,0)]

# Consultation de professionnels m�dicaux:
baro[, sa22_1 := ifelse(is.na(sa22_bd_1), sa22_ac_1, sa22_bd_1)]
baro[, sa22_2 := ifelse(is.na(sa22_bd_2), sa22_ac_2, sa22_bd_2)]
baro[, sa22_3 := ifelse(is.na(sa22_bd_3), sa22_ac_3, sa22_bd_3)]
baro[, sa22_4 := ifelse(is.na(sa22_bd_4), sa22_ac_4, sa22_bd_4)]
baro[, sa22_5 := ifelse(is.na(sa22_bd_5), sa22_ac_5, sa22_bd_5)]
baro[, sa22_6 := ifelse(is.na(sa22_bd_6), sa22_ac_6, sa22_bd_6)]
baro[, sa22_7 := ifelse(is.na(sa22_bd_7), sa22_ac_7, sa22_bd_7)]
baro[, sa22_8 := ifelse(is.na(sa22_bd_8), sa22_ac_8, sa22_bd_8)]


### Variables sp�cifiques au module 100% sant� 2021:


### Constitution de 5 groupes � partir de ME1 et ME2:
# 1 : pas besoin
# 2 : besoin et compte en b�n�ficier
# 3 : �quipements ne conviennent pas
# 4 : revenus trop �lev�s
# 5 : autre raison
# 6 : NSP



baro[annee==2021, group := ifelse(me1 == 2 & me2 == 1,1,
                                  ifelse(me1 == 1,2,
                                         ifelse(me1 == 2 & me2 == 2,3,
                                                ifelse(me1 == 2 & me2 == 3,4,
                                                       ifelse(me1 == 2 & me2 == 4,5,6)))))]

# Connaissance du 100% sant�:
baro[, connaissance := ifelse(og13_12 == 1,1,ifelse(og13_12==2,0,NA))]
baro[, qui_benef    := ifelse(is.na(og13bis_12),0,og13bis_12)]

# Besoin d'un �quipement optique, dentaire ou audio:
baro[, besoin := ifelse(group == 6,NA,ifelse(group==1,0,1))]

# Intention de recours au 100% sant�
baro[, recours := ifelse(group==2,1,ifelse(group %in% c(3,4,5),0,NA))]




################################################################################
##########                  Connaissance du 100% Sant�                ##########
################################################################################



################################# Graphique 1 ##################################



baro_2020[is.na(og13bis_1 ),og13bis_1 := 0]
baro_2020[is.na(og13bis_2 ),og13bis_2 := 0]
baro_2020[is.na(og13bis_3 ),og13bis_3 := 0]
baro_2020[is.na(og13bis_4 ),og13bis_4 := 0]
baro_2020[is.na(og13bis_5 ),og13bis_5 := 0]
baro_2020[is.na(og13bis_6 ),og13bis_6 := 0]
baro_2020[is.na(og13bis_7 ),og13bis_7 := 0]
baro_2020[is.na(og13bis_8 ),og13bis_8 := 0]
baro_2020[is.na(og13bis_9 ),og13bis_9 := 0]
baro_2020[is.na(og13bis_10),og13bis_10:= 0]
baro_2020[is.na(og13bis_11),og13bis_11:= 0]
baro_2020[is.na(og13bis_12),og13bis_12:= 0]



CONNAISSANCE_2020 <- matrix(NA,12,4)

CONNAISSANCE_2020[1 ,1] <- baro_2020[annee==2020 & og13_1 !=3, ifelse(og13bis_1 ==1,1,0)%*%poids/sum(poids)]
CONNAISSANCE_2020[2 ,1] <- baro_2020[annee==2020 & og13_2 !=3, ifelse(og13bis_2 ==1,1,0)%*%poids/sum(poids)]
CONNAISSANCE_2020[3 ,1] <- baro_2020[annee==2020 & og13_3 !=3, ifelse(og13bis_3 ==1,1,0)%*%poids/sum(poids)]
CONNAISSANCE_2020[4 ,1] <- baro_2020[annee==2020 & og13_4 !=3, ifelse(og13bis_4 ==1,1,0)%*%poids/sum(poids)]
CONNAISSANCE_2020[5 ,1] <- baro_2020[annee==2020 & og13_5 !=3, ifelse(og13bis_5 ==1,1,0)%*%poids/sum(poids)]
CONNAISSANCE_2020[6 ,1] <- baro_2020[annee==2020 & og13_6 !=3, ifelse(og13bis_6 ==1,1,0)%*%poids/sum(poids)]
CONNAISSANCE_2020[7 ,1] <- baro_2020[annee==2020 & og13_7 !=3, ifelse(og13bis_7 ==1,1,0)%*%poids/sum(poids)]
CONNAISSANCE_2020[8 ,1] <- baro_2020[annee==2020 & og13_8 !=3, ifelse(og13bis_8 ==1,1,0)%*%poids/sum(poids)]
CONNAISSANCE_2020[9 ,1] <- baro_2020[annee==2020 & og13_9 !=3, ifelse(og13bis_9 ==1,1,0)%*%poids/sum(poids)]
CONNAISSANCE_2020[10,1] <- baro_2020[annee==2020 & og13_10!=3, ifelse(og13bis_10==1,1,0)%*%poids/sum(poids)]
CONNAISSANCE_2020[11,1] <- baro_2020[annee==2020 & og13_11!=3, ifelse(og13bis_11==1,1,0)%*%poids/sum(poids)]
CONNAISSANCE_2020[12,1] <- baro_2020[annee==2020 & og13_12!=3, ifelse(og13bis_12==1,1,0)%*%poids/sum(poids)]

CONNAISSANCE_2020[1 ,2] <- baro_2020[annee==2020 & og13_1 !=3, ifelse(og13bis_1 ==2,1,0)%*%poids/sum(poids)]
CONNAISSANCE_2020[2 ,2] <- baro_2020[annee==2020 & og13_2 !=3, ifelse(og13bis_2 ==2,1,0)%*%poids/sum(poids)]
CONNAISSANCE_2020[3 ,2] <- baro_2020[annee==2020 & og13_3 !=3, ifelse(og13bis_3 ==2,1,0)%*%poids/sum(poids)]
CONNAISSANCE_2020[4 ,2] <- baro_2020[annee==2020 & og13_4 !=3, ifelse(og13bis_4 ==2,1,0)%*%poids/sum(poids)]
CONNAISSANCE_2020[5 ,2] <- baro_2020[annee==2020 & og13_5 !=3, ifelse(og13bis_5 ==2,1,0)%*%poids/sum(poids)]
CONNAISSANCE_2020[6 ,2] <- baro_2020[annee==2020 & og13_6 !=3, ifelse(og13bis_6 ==2,1,0)%*%poids/sum(poids)]
CONNAISSANCE_2020[7 ,2] <- baro_2020[annee==2020 & og13_7 !=3, ifelse(og13bis_7 ==2,1,0)%*%poids/sum(poids)]
CONNAISSANCE_2020[8 ,2] <- baro_2020[annee==2020 & og13_8 !=3, ifelse(og13bis_8 ==2,1,0)%*%poids/sum(poids)]
CONNAISSANCE_2020[9 ,2] <- baro_2020[annee==2020 & og13_9 !=3, ifelse(og13bis_9 ==2,1,0)%*%poids/sum(poids)]
CONNAISSANCE_2020[10,2] <- baro_2020[annee==2020 & og13_10!=3, ifelse(og13bis_10==2,1,0)%*%poids/sum(poids)]
CONNAISSANCE_2020[11,2] <- baro_2020[annee==2020 & og13_11!=3, ifelse(og13bis_11==2,1,0)%*%poids/sum(poids)]
CONNAISSANCE_2020[12,2] <- baro_2020[annee==2020 & og13_12!=3, ifelse(og13bis_12==2,1,0)%*%poids/sum(poids)]

CONNAISSANCE_2020[1 ,3] <- baro_2020[annee==2020 & og13_1 !=3, ifelse(og13bis_1 ==3,1,0)%*%poids/sum(poids)]
CONNAISSANCE_2020[2 ,3] <- baro_2020[annee==2020 & og13_2 !=3, ifelse(og13bis_2 ==3,1,0)%*%poids/sum(poids)]
CONNAISSANCE_2020[3 ,3] <- baro_2020[annee==2020 & og13_3 !=3, ifelse(og13bis_3 ==3,1,0)%*%poids/sum(poids)]
CONNAISSANCE_2020[4 ,3] <- baro_2020[annee==2020 & og13_4 !=3, ifelse(og13bis_4 ==3,1,0)%*%poids/sum(poids)]
CONNAISSANCE_2020[5 ,3] <- baro_2020[annee==2020 & og13_5 !=3, ifelse(og13bis_5 ==3,1,0)%*%poids/sum(poids)]
CONNAISSANCE_2020[6 ,3] <- baro_2020[annee==2020 & og13_6 !=3, ifelse(og13bis_6 ==3,1,0)%*%poids/sum(poids)]
CONNAISSANCE_2020[7 ,3] <- baro_2020[annee==2020 & og13_7 !=3, ifelse(og13bis_7 ==3,1,0)%*%poids/sum(poids)]
CONNAISSANCE_2020[8 ,3] <- baro_2020[annee==2020 & og13_8 !=3, ifelse(og13bis_8 ==3,1,0)%*%poids/sum(poids)]
CONNAISSANCE_2020[9 ,3] <- baro_2020[annee==2020 & og13_9 !=3, ifelse(og13bis_9 ==3,1,0)%*%poids/sum(poids)]
CONNAISSANCE_2020[10,3] <- baro_2020[annee==2020 & og13_10!=3, ifelse(og13bis_10==3,1,0)%*%poids/sum(poids)]
CONNAISSANCE_2020[11,3] <- baro_2020[annee==2020 & og13_11!=3, ifelse(og13bis_11==3,1,0)%*%poids/sum(poids)]
CONNAISSANCE_2020[12,3] <- baro_2020[annee==2020 & og13_12!=3, ifelse(og13bis_12==3,1,0)%*%poids/sum(poids)]

CONNAISSANCE_2020[,4]   <- CONNAISSANCE_2020[,1:3]%*%matrix(1,3,1)

write.xlsx(CONNAISSANCE_2020, file = "sorties//connaissance_2020.xls")





################################# Graphique 2 ##################################


CONNAIS_NV     <- matrix(NA,10,3)

CONNAIS_NV[,1] <- as.numeric(unlist(baro[annee>=2020 & og13_12!=3, ifelse(qui_benef==1,1,0)%*%poids/sum(poids), keyby = c("sdnivie","annee")][,3]))
CONNAIS_NV[,2] <- as.numeric(unlist(baro[annee>=2020 & og13_12!=3, ifelse(qui_benef==2,1,0)%*%poids/sum(poids), keyby = c("sdnivie","annee")][,3]))
CONNAIS_NV[,3] <- as.numeric(unlist(baro[annee>=2020 & og13_12!=3, ifelse(qui_benef==3,1,0)%*%poids/sum(poids), keyby = c("sdnivie","annee")][,3]))

write.xlsx(CONNAIS_NV, file="sorties//connaissance_nv.xls")


### Statistiques dans le texte:

baro[annee==2021 & sdsexe == 2 & !is.na(connaissance), connaissance%*%poids/sum(poids)]
baro[annee==2021 & dipl == 4 & !is.na(connaissance), connaissance%*%poids/sum(poids)]
baro[annee==2021 & sdage > 45 & !is.na(connaissance), connaissance%*%poids/sum(poids)]

baro[annee==2021 & !is.na(connaissance), connaissance%*%poids/sum(poids), keyby = og4_8]
baro[annee==2021 & !is.na(connaissance) & sa1 %in% c(4,5), connaissance%*%poids/sum(poids)]
baro[annee==2021 & !is.na(connaissance) & sa1==1 , connaissance%*%poids/sum(poids)]

baro[annee==2021 & sa14 != 6, sum(poids), keyby = sa14][,2]/baro[annee==2021 & sa14 != 6, sum(poids)]
baro[annee==2021 & !is.na(connaissance), connaissance%*%poids/sum(poids), keyby = sa14]
baro[annee==2021 & !is.na(connaissance), connaissance%*%poids/sum(poids), keyby = sa22_6]
baro[annee==2021 & !is.na(connaissance) & sa15_2 %in% c(1,2), connaissance%*%poids/sum(poids)]
baro[annee==2021 & !is.na(connaissance) & sa15_2 %in% c(3,4), connaissance%*%poids/sum(poids)]




################################################################################
##########              Besoin et renoncement aux soins               ##########
################################################################################


################################## Tableau 1 ###################################


TAB1 <- matrix(NA,4,4)

TAB1[1,1] <- baro[annee==2021 & !is.na(besoin_optic), besoin_optic%*%poids/sum(poids)]
TAB1[2,1] <- baro[annee==2021 & !is.na(besoin_dent ), besoin_dent %*%poids/sum(poids)]
TAB1[3,1] <- baro[annee==2021 & !is.na(besoin_audio), besoin_audio%*%poids/sum(poids)]
TAB1[4,1] <- baro[annee==2021 & !is.na(besoin_exam),  besoin_exam%*%poids/sum(poids)]

TAB1[1,2] <- baro[annee==2021 & !is.na(renonc_optic), renonc_optic%*%poids/sum(poids)]
TAB1[2,2] <- baro[annee==2021 & !is.na(renonc_dent ), renonc_dent %*%poids/sum(poids)]
TAB1[3,2] <- baro[annee==2021 & !is.na(renonc_audio), renonc_audio%*%poids/sum(poids)]
TAB1[4,2] <- baro[annee==2021 & !is.na(renonc_exam ), renonc_exam%*%poids/sum(poids)]

TAB1[1,3] <- baro[annee==2021 & !is.na(renonc_optic) & sdnivie==1, renonc_optic%*%poids/sum(poids)]
TAB1[2,3] <- baro[annee==2021 & !is.na(renonc_dent ) & sdnivie==1, renonc_dent %*%poids/sum(poids)]
TAB1[3,3] <- baro[annee==2021 & !is.na(renonc_audio) & sdnivie==1, renonc_audio%*%poids/sum(poids)]
TAB1[4,3] <- baro[annee==2021 & !is.na(renonc_exam ) & sdnivie==1, renonc_exam%*%poids/sum(poids)]

TAB1[1,4] <- baro[annee==2021 & !is.na(renonc_optic) & sdnivie==5, renonc_optic%*%poids/sum(poids)]
TAB1[2,4] <- baro[annee==2021 & !is.na(renonc_dent ) & sdnivie==5, renonc_dent %*%poids/sum(poids)]
TAB1[3,4] <- baro[annee==2021 & !is.na(renonc_audio) & sdnivie==5, renonc_audio%*%poids/sum(poids)]
TAB1[4,4] <- baro[annee==2021 & !is.na(renonc_exam ) & sdnivie==5, renonc_exam%*%poids/sum(poids)]

write.xlsx(TAB1, file="sorties//tableau_1.xls")


################################################################################
##########             Besoin & recours au 100% Sant�                 ##########
################################################################################


### Statistiques dans le texte:


baro[annee==2021 & group!=6, sum(poids), keyby = group][,2]/baro[annee==2021 & group!=6, sum(poids)]
baro[annee==2021 & connaissance == 1 & besoin_equip == 1, sum(poids)]/baro[annee==2021 & !is.na(connaissance) & !is.na(besoin_equip), sum(poids)]
baro[annee==2021 & connaissance == 1 & besoin_equip == 1 & recours == 1, sum(poids)]/baro[annee==2021 & !is.na(connaissance) & !is.na(recours) & !is.na(besoin_equip), sum(poids)]
baro[annee==2021 & connaissance == 1 & besoin_equip == 1 & recours == 1, sum(poids)]/baro[annee==2021 & !is.na(connaissance) & !is.na(besoin_equip) , sum(poids)]


baro[annee==2021 & group==1, poids%*%sdage/sum(poids)]
baro[annee==2021 & !(group%in%c(1,6)), poids%*%sdage/sum(poids)]
baro[annee==2021 & !is.na(etat_sante) & group == 1, poids%*%etat_sante/sum(poids)]
baro[annee==2021 & !is.na(etat_sante) & !(group%in%c(1,6)), poids%*%etat_sante/sum(poids)]
baro[annee==2021 & !is.na(besoin_exam) & group == 1,         poids%*%besoin_exam/sum(poids)]
baro[annee==2021 & !is.na(besoin_exam) & !(group%in%c(1,6)), poids%*%besoin_exam/sum(poids)]
baro[annee==2021 & sa22_4!=4 & group == 1,         poids%*%ifelse(sa22_4==1,1,0)/sum(poids)]
baro[annee==2021 & sa22_4!=4 & !(group%in%c(1,6)), poids%*%ifelse(sa22_4==1,1,0)/sum(poids)]
baro[annee==2021 & sa22_6!=4 & group == 1,         poids%*%ifelse(sa22_6==1,1,0)/sum(poids)]
baro[annee==2021 & sa22_6!=4 & !(group%in%c(1,6)), poids%*%ifelse(sa22_6==1,1,0)/sum(poids)]



################################# Graphique 3 ##################################


RECOURS <- c()
RECOURS[1] <- baro[annee==2021 & !is.na(recours) & og13_12 == 2                  , recours%*%poids/sum(poids)]
RECOURS[2] <- baro[annee==2021 & !is.na(recours) & og13_12 == 1 & og13bis_12 == 3, recours%*%poids/sum(poids)]
RECOURS[3] <- baro[annee==2021 & !is.na(recours) & og13_12 == 1 & og13bis_12 == 2, recours%*%poids/sum(poids)]
RECOURS[4] <- baro[annee==2021 & !is.na(recours) & og13_12 == 1 & og13bis_12 == 1, recours%*%poids/sum(poids)]
RECOURS[5] <- baro[annee==2021 & !is.na(recours)                                 , recours%*%poids/sum(poids)]

write.xlsx(RECOURS,file="sorties//graphique_3.xls")


################################# Graphique 4 ##################################

NV <- matrix(NA,5,6)

NV[,1] <- as.numeric(unlist(baro[annee==2021 & group!=6 & sdnivie == 1, sum(poids), keyby = group][,2]))/baro[annee==2021 & group!=6 & sdnivie == 1, sum(poids)]
NV[,2] <- as.numeric(unlist(baro[annee==2021 & group!=6 & sdnivie == 2, sum(poids), keyby = group][,2]))/baro[annee==2021 & group!=6 & sdnivie == 2, sum(poids)]
NV[,3] <- as.numeric(unlist(baro[annee==2021 & group!=6 & sdnivie == 3, sum(poids), keyby = group][,2]))/baro[annee==2021 & group!=6 & sdnivie == 3, sum(poids)]
NV[,4] <- as.numeric(unlist(baro[annee==2021 & group!=6 & sdnivie == 4, sum(poids), keyby = group][,2]))/baro[annee==2021 & group!=6 & sdnivie == 4, sum(poids)]
NV[,5] <- as.numeric(unlist(baro[annee==2021 & group!=6 & sdnivie == 5, sum(poids), keyby = group][,2]))/baro[annee==2021 & group!=6 & sdnivie == 5, sum(poids)]
NV[,6] <- as.numeric(unlist(baro[annee==2021 & group!=6, sum(poids), keyby = group][,2]))/baro[annee==2021 & group!=6, sum(poids)]

write.xlsx(NV, file = "sorties//graphique_4.xls")


################################# Graphique 5 ##################################

ES <- matrix(NA,5,5)


ES[,1] <- as.numeric(unlist(baro[annee==2021 & group!=6 & sa1 == 1, sum(poids), keyby = group][,2]))/baro[annee==2021 & group!=6 & sa1 == 1, sum(poids)]
ES[,2] <- as.numeric(unlist(baro[annee==2021 & group!=6 & sa1 == 2, sum(poids), keyby = group][,2]))/baro[annee==2021 & group!=6 & sa1 == 2, sum(poids)]
ES[,3] <- as.numeric(unlist(baro[annee==2021 & group!=6 & sa1 == 3, sum(poids), keyby = group][,2]))/baro[annee==2021 & group!=6 & sa1 == 3, sum(poids)]
ES[,4] <- as.numeric(unlist(baro[annee==2021 & group!=6 & sa1 %in% c(4,5), sum(poids), keyby = group][,2]))/baro[annee==2021 & group!=6 & sa1 %in% c(4,5), sum(poids)]
ES[,5] <- as.numeric(unlist(baro[annee==2021 & group!=6, sum(poids), keyby = group][,2]))/baro[annee==2021 & group!=6, sum(poids)]

write.xlsx(ES, file = "sorties//graphique_5.xls")



######################### Graphique compl�mentaire A ###########################

AGE <- matrix(NA,5,5)


AGE[,1] <- as.numeric(unlist(baro[annee==2021 & group!=6 & age == 1, sum(poids), keyby = group][,2]))/baro[annee==2021 & group!=6 & age == 1, sum(poids)]
AGE[,2] <- as.numeric(unlist(baro[annee==2021 & group!=6 & age == 2, sum(poids), keyby = group][,2]))/baro[annee==2021 & group!=6 & age == 2, sum(poids)]
AGE[,3] <- as.numeric(unlist(baro[annee==2021 & group!=6 & age == 3, sum(poids), keyby = group][,2]))/baro[annee==2021 & group!=6 & age == 3, sum(poids)]
AGE[,4] <- as.numeric(unlist(baro[annee==2021 & group!=6 & age == 4, sum(poids), keyby = group][,2]))/baro[annee==2021 & group!=6 & age == 4, sum(poids)]
AGE[,5] <- as.numeric(unlist(baro[annee==2021 & group!=6, sum(poids), keyby = group][,2]))/baro[annee==2021 & group!=6, sum(poids)]

write.xlsx(AGE, file = "sorties//graphique_A.xls")


### Statistiques dans le texte:

baro[annee == 2021 & !is.na(recours), recours%*%poids/sum(poids), keyby = sa17]
baro[annee == 2021 & !is.na(besoin_equip), besoin_equip%*%poids/sum(poids), keyby = renonc_100p]
baro[annee == 2021 & !is.na(recours), recours%*%poids/sum(poids), keyby = renonc_100p]
baro[annee == 2021 & !is.na(recours), recours%*%poids/sum(poids), keyby = renonc_100p]



################################################################################
##########                    R�gression encadr� 2                    ##########
################################################################################



baro[, c("dipl_reg","habitat_reg","famille_reg","nv_reg","etat_sante_reg","age_reg",
         "log_reg","sdpcs10_reg","enf_reg") := 
       lapply(.SD,as.factor),
     .SDcols = c("dipl","habitat","famille","sdnivie","etat_sante","age","log","sdpcs10","enf")]


baro[, sexe_reg := as.factor(sdsexe-1)]
baro[, couple_reg := as.factor(sdcouple-1)]

### Pour obtenir directement en pp:
baro[, recours_reg := 100*recours]
baro[, connaissance_reg := 100*connaissance]
baro[, besoin_reg := 100*besoin]


baro[, etat_sante:=as.factor(ifelse(sa1<=3,sa1,ifelse(sa1==6,NA,4)))]

explicatives <- "sexe_reg + dipl_reg + habitat_reg + couple_reg + enf_reg + nv_reg + age_reg + log_reg"



RES <- matrix(NA,22,9)

RES[,1:2] <- summary(lm(as.formula(paste0("connaissance_reg ~ ", explicatives)), baro[annee==2021 & !is.na(connaissance)]))$coeff[,1:2]
RES[,4:5] <- summary(lm(as.formula(paste0("besoin_reg ~ ", explicatives)),       baro[annee==2021 & !is.na(besoin)]))$coeff[,1:2]
RES[,7:8] <- summary(lm(as.formula(paste0("recours_reg ~ ", explicatives)),      baro[annee==2021 & !is.na(recours)]))$coeff[,1:2]
RES[,3]   <- stars(summary(lm(as.formula(paste0("connaissance_reg ~ ", explicatives)), baro[annee==2021 & !is.na(connaissance)]))$coeff[,4])
RES[,6]   <- stars(summary(lm(as.formula(paste0("besoin_reg ~ ", explicatives)), baro[annee==2021 & !is.na(besoin)]))$coeff[,4])
RES[,9]   <- stars(summary(lm(as.formula(paste0("recours_reg ~ ", explicatives)), baro[annee==2021 & !is.na(recours)]))$coeff[,4])

rownames(RES) <- rownames(summary(lm(as.formula(paste0("connaissance_reg ~ ", explicatives)), baro[annee==2021 & !is.na(connaissance)]))$coeff)


write.xlsx(RES, file="sorties//regressions.xls")






