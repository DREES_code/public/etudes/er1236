# ER1236
Ce dossier fournit le programme `code_100sante.R` qui permet de reproduire la méthodologie et les figures de l'Études et résultats n° 1236 de la DREES : *« 100% santé » : fin 2021, un peu plus de la moitié des Français en ont entendu parler*, juillet 2022.

L’utilisateur remplacera la localisation de l’environnement de travail **setwd** par son propre chemin d’accès aux programmes et **path** par le chemin d’accès aux données du Baromètre. Les sorties sont exportées dans le dossier **sorties** créé au sein de l'environnement de travail.


Lien vers l'étude : https://drees.solidarites-sante.gouv.fr/publications-communique-de-presse/etudes-et-resultats/100-sante-fin-2021-un-peu-plus-de-la-moitie

La Direction de la recherche, des études, de l’évaluation et des statistiques (Drees) est une direction de l’administration centrale des ministères sanitaires et sociaux. https://drees.solidarites-sante.gouv.fr/etudes-et-statistiques/la-drees/qui-sommes-nous/ 

Source de données : Baromètre d'opinion de la DREES 2020-2021. Traitements : Drees. Une présentation du Baromètre, les questionnaires et les jeux de données sont disponibles en accès libre : https://drees.solidarites-sante.gouv.fr/sources-outils-et-enquetes/le-barometre-dopinion-de-la-drees


Les programmes ont été exécutés pour la dernière fois avec le logiciel R version 4.1.2, le 07/07/2022.
